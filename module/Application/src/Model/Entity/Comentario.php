<?php
namespace Application\Model\Entity;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Comentario
 *
 * @author Guillermo
 */
class Comentario {
    //put your code here
    private $propietario;
    private $comentarioTexto;
    private $fechaCreacion;
    private $idArticulo;
    
    
    public function __construct($propietario = null, $comentarioTexto = null, $fechaCreacion = null, $idArticulo = null) {
        $this->propietario = $propietario;
        $this->comentarioTexto = $comentarioTexto;
        $this->fechaCreacion = $fechaCreacion;
        $this->idArticulo = $idArticulo;
    }
    
    function getPropietario() {
        return $this->propietario;
    }

    function getComentarioTexto() {
        return $this->comentarioTexto;
    }

    function getFechaCreacion() {
        return $this->fechaCreacion;
    }

    function getIdArticulo() {
        return $this->idArticulo;
    }

    function setPropietario($propietario) {
        $this->propietario = $propietario;
    }

    function setComentarioTexto($comentarioTexto) {
        $this->comentarioTexto = $comentarioTexto;
    }

    function setFechaCreacion($fechaCreacion) {
        $this->fechaCreacion = $fechaCreacion;
    }

    function setIdArticulo($idArticulo) {
        $this->idArticulo = $idArticulo;
    }
    
    public function exchangeArray($data) {
        date_default_timezone_set("America/Mexico_City");
    
        $this->propietario = (isset($data['propietario'])) ? $data['propietario'] : null;
        $this->comentarioTexto = (isset($data['comentarioTexto'])) ? $data['comentarioTexto'] : null;
        $this->fechaCreacion = (isset($data['fechaCreacion'])) ? $data['fechaCreacion'] : date("Y-m-d");
        $this->idArticulo = (isset($data['idArticulo'])) ? $data['idArticulo'] : null;
    }

    public function getArrayCopy() {
        return get_object_vars($this);
    }


}

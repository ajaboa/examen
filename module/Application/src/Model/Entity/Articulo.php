<?php

namespace Application\Model\Entity;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Articulo
 *
 * @author Guillermo
 */
class Articulo {
    //put your code here
    private $propietario;
    private $articuloTexto;
    private $foto;
    private $fechaCreacion;
    
    
    public function __construct($propietario = null, $articuloTexto = null, $foto = null, $fechaCreacion = null) {
        $this->propietario = $propietario;
        $this->articuloTexto = $articuloTexto;
        $this->foto = $foto;
        $this->fechaCreacion = $fechaCreacion;
    }
    
    
    function getPropietario() {
        return $this->propietario;
    }

    function getArticuloTexto() {
        return $this->articuloTexto;
    }

    function getFoto() {
        return $this->foto;
    }

    function getFechaCreacion() {
        return $this->fechaCreacion;
    }

    function setPropietario($propietario) {
        $this->propietario = $propietario;
    }

    function setArticuloTexto($articuloTexto) {
        $this->articuloTexto = $articuloTexto;
    }

    function setFoto($foto) {
        $this->foto = $foto;
    }

    function setFechaCreacion($fechaCreacion) {
        $this->fechaCreacion = $fechaCreacion;
    }
    
    public function exchangeArray($data) {
        date_default_timezone_set("America/Mexico_City");
        $this->propietario = (isset($data['propietario'])) ? $data['propietario'] : null;
        $this->articuloTexto = (isset($data['articuloTexto'])) ? $data['articuloTexto'] : null;
        $this->foto = (isset($data['foto'])) ? $data['foto'] : null;
        $this->fechaCreacion = (isset($data['fechaCreacion'])) ? $data['fechaCreacion'] : date("Y-m-d");
    }

    public function getArrayCopy() {
        return get_object_vars($this);
    }


}

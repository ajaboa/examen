<?php

namespace Application\Model\Dao;

use Application\Model\Entity\Comentario;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Guillermo
 */
interface IComentarioDao {
    //put your code here
    public function obtenerTodos();

    public function obtenerPorId($id);

    public function guardar(Comentario $comentario);

    public function eliminar(Comentario $comentario);
}

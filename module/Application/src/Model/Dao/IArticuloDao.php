<?php
namespace Application\Model\Dao;

use Application\Model\Entity\Articulo;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Guillermo
 */
interface IArticuloDao {
    //put your code here
    public function obtenerTodos();

    public function obtenerPorId($id);

    public function guardar(Articulo $articulo);

    public function eliminar(Articulo $articulo);

}

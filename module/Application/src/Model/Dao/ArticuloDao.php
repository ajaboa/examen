<?php

namespace Application\Model\Dao;

use Zend\Db\TableGateway\TableGateway;
use Application\Model\Entity\Articulo;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ArticuloDao
 *
 * @author Guillermo
 */
class ArticuloDao implements IArticuloDao{
    //put your code here
    
    protected $tableGateway;

    public function __construct(TableGateway $tableGateway) {
        $this->tableGateway = $tableGateway;
    }

    public function obtenerTodos() {
        return $this->tableGateway->select();
    }

    public function obtenerPorId($id) {
        $id = (int) $id;
        $rowset = $this->tableGateway->select(array('id' => $id));
        $row = $rowset->current();

        if (!$row) {
            throw new \Exception("Could not find row $id");
        }
        return $row;
    }

    public function eliminar(Articulo $articulo) {
        $this->tableGateway->delete(array('id' => $articulo->getId()));
    }

    public function guardar(Articulo $articulo) {
        $data = array(
            'propietario' => $articulo->getPropietario(),
            'articuloTexto' => $articulo->getArticuloTexto(),
            'foto' => $articulo->getFoto(),
            'fechaCreacion' => $articulo->getFechaCreacion(),
        );

        $id = (int) $articulo->getId();

        if ($id == 0) {
            $this->tableGateway->insert($data);
        } else {
            if ($this->obtenerPorId($id)) {
                $this->tableGateway->update($data, array('id' => $id));
            } else {
                throw new \Exception('El articulo con el id no existe');
            }
        }
    }

}

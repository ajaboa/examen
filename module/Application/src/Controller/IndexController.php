<?php
/**
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2016 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Application\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Application\Model\Entity\Articulo;

class IndexController extends AbstractActionController
{
    private $articuloDao;
    private $comentarioDao;
    private $config;

    function __construct($config = null) {
        $this->config = $config;
    }
    
    function getArticuloDao() {
        return $this->articuloDao;
    }

    function getComentarioDao() {
        return $this->comentarioDao;
    }

    function setArticuloDao($articuloDao) {
        $this->articuloDao = $articuloDao;
    }

    function setComentarioDao($comentarioDao) {
        $this->comentarioDao = $comentarioDao;
    }

        
    public function indexAction()
    {
        return new ViewModel();
    }
    
    public function guardarAction()
    {
        $data = $this->getRequest()->getPost();
        
        $articulo = new Articulo();
        $articulo->exchangeArray($data);
        
        
        
        $this->getArticuloDao()->guardar($articulo);
        
        return $this->redirect()->toRoute('application',array('controller'=>'index','action'=>'ver'));
        
    }
    
    public function verAction(){
        return new ViewModel();
    }
}

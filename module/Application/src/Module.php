<?php
/**
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2016 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Application;


use Zend\ModuleManager\Feature\AutoloaderProviderInterface;
use Zend\ModuleManager\Feature\ConfigProviderInterface;

use Zend\ModuleManager\Feature\ServiceProviderInterface;
use Zend\ModuleManager\Feature\ControllerProviderInterface;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\TableGateway\TableGateway;
use Application\Model\Entity\Articulo;
use Application\Model\Entity\Comentario;
use Application\Model\Dao\ArticuloDao;
use Application\Model\Dao\ComentarioDao;

class Module implements ServiceProviderInterface, ControllerProviderInterface, ConfigProviderInterface, AutoloaderProviderInterface
{
    const VERSION = '3.1.4dev';

    public function getConfig()
    {
        return include __DIR__ . '/../config/module.config.php';
    }
    
    public function getAutoloaderConfig() {
        return array(
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
                ),
            ),
        );
    }
    
    public function getServiceConfig() {
        return array(
            'factories' => array(
                'Articulo\Model\ArticuloDao' => function($sm) {
                    $tableGateway = $sm->get('ArticuloTableGateway');
                    $dao = new ArticuloDao($tableGateway);
                    return $dao;
                },
                'ArticuloTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new Articulo());
                    return new TableGateway('articulo', $dbAdapter, null, $resultSetPrototype);
                },
                'Comentario\Model\ComentarioDao' => function($sm) {
                    $tableGateway = $sm->get('ComentarioTableGateway');
                    $dao = new ComentarioDao($tableGateway);
                    return $dao;
                },
                'ComentarioTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new Comentario());
                    return new TableGateway('comentario', $dbAdapter, null, $resultSetPrototype);
                },
            ),
        );
    }
    
    public function getControllerConfig() {
        return array(
            'factories' => array(
                'Application\Controller\Index' => function ($sm) {

                    $locator = $sm->getServiceLocator();
                    $config = null;

                    $articuloDao = $locator->get('Articulo\Model\ArticuloDao');
                    $comentarioDao = $locator->get('Comentario\Model\ComentarioDao');

                    $controller = new \Application\Controller\IndexController($config);
                    $controller->setArticuloDao($articuloDao);
                    $controller->setComentarioDao($comentarioDao);
                    return $controller;
                }
            )
        );
    }
}
